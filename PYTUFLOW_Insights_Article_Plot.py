# DRK 13/05/2022
# Imports data, converts Date/Time to Datetime.
# plots data on seperate graphs using same X-axis
# Uses plotly and shows rainfall and flow data.

import datetime
import pytuflow
import plotly.graph_objects as go
from plotly.subplots import make_subplots
import pandas as pd
from pandas import DataFrame

# Read in data from csv files
df = pd.read_csv(r'C:\Data\Flow_All.csv')
df['TIMESTAMP'] = pd.to_datetime(df['TIMESTAMP'], dayfirst=True)  # Converts the date time string to datetime.  Dayfirst is needed to set DD/mm/YYYY format

# Read in data from csv files
df_rf = pd.read_csv(r'C:\Data\Rainfall_All.csv')
df_rf['TIMESTAMP'] = pd.to_datetime(df_rf['TIMESTAMP'],
                                    dayfirst=True)  # Converts the date time string to datetime.  Dayfirst is needed to set DD/mm/YYYY format

# Set up initial figure
fig = make_subplots(specs=[[{"secondary_y": True}]])

# Add Observed flow to plot
fig.add_trace(go.Scatter(x=df['TIMESTAMP'], y=df['BODMIN DUNMERE'], name='Bodmin Dunmere', marker_color="black", mode='lines'))
# Add Observed rainfall to plot
fig.add_trace(go.Bar(x=df_rf['TIMESTAMP'], y=df_rf['BODMIN CALLYWITH'], name='Rainfall', marker_color='rgb(26, 118, 255)', opacity=0.8), secondary_y=True)

# Load in TUFLOW Simulation results into a list
FNAMES = []
FNAMES.append(
    r'C:\Data\Model_Results.tpc')  # Change these to add new TUFLOW result

# Define model Start Time and Plot Start/End Time
start_date = datetime.datetime(2020, 2, 12, 12, 0, 0)  # Change this to start datetime of the model time zero
plt_start_date = datetime.datetime(2020, 2, 12, 0, 0, 0)  # Defines beginning of plot
plt_end_date = datetime.datetime(2020, 2, 17, 12, 0, 0)  # Defines end of plot

res = pytuflow.ResData()

# Function to read and plot TUFLOW PO Q Results
def graph_po_line (tpc, po_id):
    err, message = res.load(tpc)
    if not err:
        # get time series data and add to plot
        result_type = 'Q'
        err, message, data = res.getTimeSeriesData(po_id, result_type, '2D')
        # check for error before plotting
        if not err:
            x, y = data
            df1 = DataFrame(x, columns=['Time'])
            df['datetime_Model'] = start_date + pd.to_timedelta(df1.pop('Time'),
                                                                unit='h')  # Converts the TUFLOW model time to datetime
            x1 = df['datetime_Model']
            res_name = res.name()
            fig.add_trace(go.Scatter(x=x1, y=y,
                                     mode='lines',
                                     name=f'{res_name}{po_id}'))
        else:
            # error occurred getting time series data, print error message to console
            print(message)

# user input to add PO reference.
for fname in FNAMES:
    graph_po_line(fname, 'Dun_Cha_L')

# Configure Y/X Axis Ranges
fig.update_xaxes(range=(plt_start_date, plt_end_date))
fig.update_yaxes(range=(0, 90))
fig.update_yaxes(range=(20, 0), secondary_y=True)

# Label Axes
fig.update_xaxes(title_text='Date')
fig.update_yaxes(title_text="Flow m<sup>3</sup> s<sup>-1</sup>", secondary_y=False)
fig.update_yaxes(title_text="Rainfall(mm)", secondary_y=True)

# Add title and format
fig.update_layout(title={'text': 'PyTUFLOW Example-Calibration',
                         'y': 0.95,
                         'x': 0.5,
                         'xanchor': 'center',
                         'yanchor': 'top',
                         }, titlefont=dict(size=24))

# Add legend and format
fig.update_layout(
    legend=dict(
        x=1,
        y=0.5,
        bordercolor="Black",
        borderwidth=2))

fig.update_layout(template="plotly_white")

# Show Graph
fig.show()

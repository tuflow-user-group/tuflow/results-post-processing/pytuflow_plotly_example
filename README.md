# PyTUFLOW_Plotly_Example

An example described here of how to plot TUFLOW results using PyTUFLOW and Plotly.  The script requires the installation of PyTUFLOW, plotly and pandas.  A description of the script can be found here: 

# Data

The data.zip files contains the observed and model data to replicate the plots.  The script is currently configured to read the data from C:\Data, therefore the file can be unzipped to this location and the script should pick this up.  If the data is stored elsewhere, then lines 14,18 and 33 need updating to reference the correct location of the data files.
